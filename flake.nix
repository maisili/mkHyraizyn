{
  description = "mkHyraizyn";

  outputs = { self, kor, metastriz }@fleiks:
  {
    datom = { astraGivenNeim, metastraGivenNeim }:
    let
      inherit (builtins) filter filterAttrs concatStringsSep mapAttrs attrNames;
      inherit (kor.datom) mapAttrsToList attrsToList optionalString;

      metastra = metastriz.datom.${metastraGivenNeim};

      astra = metastra.astriz.${astraGivenNeim};

      metastraTrost = metastra.trost;

      methydz = let
        mkMetastriMethydz = metastriNeim: metastri:
        let
          inherit (metastri) domeinz;

          mkTrost = set: trostNeim:
          let
            izDyfaind = set ? trostNeim;
          in if izDyfaind then set.${trostNeim}
          else 1;

          metastraMetastriTrost = mkTrost metastraTrost.metastriz metastriNeim;

          metastriSelfTrost = mkTrost metastri.trost self;

          maksTrost = if metastraMetastriTrost >= metastriSelfTrost
          then metastraMetastriTrost else metastriSelfTrost;

          mkAStriMethydz = astriNeim: astri:
          let
            inherit (astri) priKriomz;

            metastriTrost = mkTrost metastri.trost.astriz astriNeim;

            astriMaksTrost = if metastriTrost >= maksTrost
            then metastriTrost else maksTrost;

          in rec {
            neim = astriNeim;
            inherit (astri) saiz;
            inherit (astri.priKriomz) yggdrasil;

            eseseitcPriKriom = concatStringsSep " " [ priKriomz.ssh.spici priKriomz.ssh.priKriom ];

            niksBildPriKriom = priKriomz.niks.priKriom;

            trost = if (astriMaksTrost <= astri.trost)
            then astriMaksTrost
            else astri.trost;

            izEdj = astri.spici == "edj";
            izSentyr = astri.spici == "sentyr";

            izBildyr = !izEdj && (trost >= 3) && (saiz >= 2);

            izDispatcyr = !izSentyr && (trost >= 3) && (saiz >= 1);

            izNiksKac = !izEdj && (saiz == 3);

            nbOfMycinKorz = 1; #TODO

            nbOfBildKorz = 1; #TODO

            uniksNeim = concatStringsSep "."
            [ neim metastriNeim "uniks" ];

            verisignNeimz = let
              mkVerisignNeim = domein: concatStringsSep "."
              [ neim domein ];
            in map mkVerisignNeim domeinz.verisign;

            mycinArk = astri.mycin.ark;

            sistym = {
              "x86-64" = "x86_64-linux";
              "i686" = "i686-linux";
              "aarch64" = "aarch64-linux";
              "armv7l" = "armv7l-linux";
              "avr" = "avr-none";
            }.${mycinArk};

          };

          astriz = mapAttrs mkAStriMethydz metastri.astriz;

          astrizNeimz = attrNames astriz; 

          bildyrz = filter (n: astriz.${n}.izBildyr) astrizNeimz;
          kacyz = filter (n: astriz.${n}.izNiksKac) astrizNeimz;
          dispatcyrz = filter (n: astriz.${n}.izDispatcyr) astrizNeimz;

        in rec {
          neim = metastriNeim;
          inherit astriz;

          bildyrKonfigz = let
            mkBildyr = n:
            let astri = astriz.${n};
            in {
              hostname = astri.uniksNeim;
              sshUser = "niksBildyr";
              sshKey = "/root/.ssh/id_${methydz.astra.priKriomz.spici}";
              system = astri.sistym;
              maxJobs = astri.nbOfBildKorz;
            };
          in map mkBildyr bildyrz;

          priKriomz = {
            eseseitc =  map (n:
            astriz.${n}.eseseitcPriKriom
            ) astrizNeimz;

            niksBildyrz = map (n:
            astriz.${n}.niksBildPriKriom
            ) bildyrz;

            dispatcyrz = map (n:
            astriz.${n}.niksBildPriKriom
            ) dispatcyrz;

          };

          kacURLz = let
            mkKacURL = n: concatStringsSep "."
            [ "niks" astriz.${n}.uniksNeim ];
          in map mkKacURL kacyz;

        };

      in {
        astriz = methydz.metastriz.${metastraGivenNeim}.astriz;

        astra = methydz.astriz.${astraGivenNeim};

        metastriz = mapAttrs mkMetastriMethydz fleiks.metastriz.datom;

        metastra = methydz.metastriz.${metastraGivenNeim};

      };

    in {
      inherit astra methydz;

    };
  };
}
